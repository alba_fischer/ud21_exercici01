package views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.GridLayout;

import java.awt.SystemColor;
import java.awt.Component;
import javax.swing.SwingConstants;
import java.awt.Font;


public class AplicacioGrafica extends JFrame{

	public static JTextField textField;
	private String text, historial;
	private double operador1, operador2;
	private String simbol = " ";
	private static double resultat;
	private static JTextField textField_1;
	DecimalFormat decimal = new DecimalFormat("0.00");
	private JTextArea textHistorial;
	
	public AplicacioGrafica() {
		setTitle("Calculadora Estàndard");
		setBounds(100, 100, 917, 721);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 137, 509, 537);
		getContentPane().add(panel);
		panel.setLayout(new GridLayout(0, 4, 0, 0));
		
		JButton boto1 = new JButton("%");
		presionarBoto(boto1);
		panel.add(boto1);
		
		JButton boto2 = new JButton("CE");
		boto2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText("0");
				operador1 = 0;
				operador2 = 0;
				simbol = " ";
			}
		});
		panel.add(boto2);
		
		JButton boto3 = new JButton("C");
		boto3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.setText("0");
				textField_1.setText(" ");
				operador1 = 0;
				simbol = " ";
				operador2 = 0;
			}
		});
		panel.add(boto3);
		
		JButton boto4 = new JButton("←");
		boto4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!textField.getText().equals("")) {								
					char num[] = new char[(textField.getText().length()-1)];					
					for (int i = 0; i < (textField.getText().length()-1); i++) {
						num[i] = textField.getText().charAt(i);
					}
					textField.setText(new String(num));	
					if (textField.getText().equals("")) {
						textField.setText("0");
					}
								
				}else {
					textField.setText("0");
				}
			}
		});
		panel.add(boto4);
		
		JButton boto5 = new JButton("x^2");
		presionarBoto(boto5);
		panel.add(boto5);
		
		JButton boto0 = new JButton("1/x");
		presionarBoto(boto0);
		panel.add(boto0);
		
		JButton boto6 = new JButton("√x");
		presionarBoto(boto6);
		panel.add(boto6);
		
		JButton boto7 = new JButton("/");
		presionarBoto (boto7);
		panel.add(boto7);
		
		JButton boto8 = new JButton("7");
		presionarBoto(boto8);
		boto8.setBackground(Color.WHITE);
		panel.add(boto8);
		
		JButton boto9 = new JButton("8");
		boto9.setBackground(Color.WHITE);
		presionarBoto(boto9);
		panel.add(boto9);
		
		
		JButton boto10 = new JButton("9");
		boto10.setBackground(Color.WHITE);
		presionarBoto(boto10);
		panel.add(boto10);
		
		JButton boto11 = new JButton("x");
		presionarBoto (boto11);
		panel.add(boto11);
		
		JButton boto12 = new JButton("4");
		boto12.setBackground(Color.WHITE);
		presionarBoto(boto12);
		panel.add(boto12);
		
		JButton boto13 = new JButton("5");
		boto13.setBackground(Color.WHITE);
		presionarBoto(boto13);
		panel.add(boto13);
		
		JButton boto14 = new JButton("6");
		boto14.setBackground(Color.WHITE);
		presionarBoto(boto14);
		panel.add(boto14);
		
		JButton boto15 = new JButton("-");
		presionarBoto (boto15);
		panel.add(boto15);
		
		JButton boto16 = new JButton("1");
		boto16.setBackground(Color.WHITE);
		presionarBoto(boto16);
		panel.add(boto16);
		
		JButton boto17 = new JButton("2");
		boto17.setBackground(Color.WHITE);
		presionarBoto(boto17);
		panel.add(boto17);
		
		JButton boto18 = new JButton("3");
		boto18.setBackground(Color.WHITE);
		presionarBoto(boto18);
		panel.add(boto18);
		
		JButton boto19 = new JButton("+");
		presionarBoto(boto19);
		panel.add(boto19);
		
		JButton boto20 = new JButton("+/-");
		presionarBoto(boto20);
		panel.add(boto20);
		
		JButton boto21 = new JButton("0");
		boto21.setBackground(Color.WHITE);
		presionarBoto(boto21);
		panel.add(boto21);
		
		JButton boto22 = new JButton(".");
		presionarBoto(boto22);
		panel.add(boto22);
		
		JButton boto23 = new JButton("=");
		presionarBoto (boto23);
		boto23.setBackground(SystemColor.activeCaption);
		panel.add(boto23);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 30));
		textField.setHorizontalAlignment(SwingConstants.RIGHT);
		textField.setEnabled(false);
		textField.setText("0");
		textField.setAlignmentX(Component.RIGHT_ALIGNMENT);
		textField.setEditable(false);
		textField.setBounds(12, 66, 497, 41);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		
		JLabel lblNewLabel = new JLabel("Historial");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lblNewLabel.setBounds(523, 19, 155, 41);
		getContentPane().add(lblNewLabel);
		
		textField_1 = new JTextField();
		textField_1.setBorder(null);
		textField_1.setText(" ");
		textField_1.setHorizontalAlignment(SwingConstants.RIGHT);
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textField_1.setEnabled(false);
		textField_1.setEditable(false);
		textField_1.setColumns(10);
		textField_1.setAlignmentX(1.0f);
		textField_1.setBounds(12, 13, 497, 41);
		getContentPane().add(textField_1);
		
		textHistorial = new JTextArea();
		textHistorial.setBounds(521, 66, 366, 553);
		textHistorial.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textHistorial.setText("");
		textHistorial.setEnabled(false);
		textHistorial.setEditable(false);
		textHistorial.setColumns(10);
		
		getContentPane().add(textHistorial);
		
		JButton btnNewButton = new JButton("X");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textHistorial.setText("");
			}
		});
		btnNewButton.setBounds(837, 632, 50, 29);
		getContentPane().add(btnNewButton);
		
	}
		
	private void presionarBoto (JButton boto) {
			boto.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (textField.getText().equals("No és pot dividir un nombre per 0") || (textField.getText().equals("No existeix √x d'un nombre negatiu"))) {
						textField.setText("0");
					}
					operadors(boto);
					text(boto);
					if (boto.getText() == ("+") || boto.getText() == ("-") || boto.getText() == ("x") || boto.getText() == ("/")) {
						if (simbol.equals(boto.getText())) {
							textField.setText("");
						}else {
							char num[] = new char[(textField_1.getText().length()-1)];					
							for (int i = 0; i < (textField_1.getText().length()-1); i++) {
								num[i] = textField_1.getText().charAt(i);
							}
							textField.setText(new String(num));	
							if (textField_1.getText().equals("")) {
								textField.setText("0");
							}
							simbol = " ";
						}
						
					}
					if (boto.getText() == ("√x")) {
						if (simbol.equals("x") || simbol.equals("-") || simbol.equals("+") || simbol.equals("/")) {
							char num[] = new char[(textField_1.getText().length()-1)];					
							for (int i = 0; i < (textField_1.getText().length()-1); i++) {
								num[i] = textField_1.getText().charAt(i);
							}
							textField.setText(new String(num));	
							if (textField_1.getText().equals("")) {
								textField.setText("0");
							}
							simbol = "√x";
							resultat(operador1, operador2, simbol);
							
							if (String.valueOf(resultat) == String.valueOf(Double.NaN)) {
								
								textField.setText("No existeix √x d'un nombre negatiu");
							}else {
								textField_1.setText(" √ " + textField.getText() + " =");
								textField.setText(String.valueOf(resultat));
								historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
								textHistorial.setText(historial);
							}
						}else {
							simbol = "√x";
							resultat(operador1, operador2, simbol);
							if (String.valueOf(resultat) == String.valueOf(Double.NaN)) {
								textField.setText("No existeix √x d'un nombre negatiu");
							}else {
							textField_1.setText(" √ " + textField.getText() + " =");
							textField.setText(String.valueOf(resultat));
							historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
							textHistorial.setText(historial);
						}
					}
					simbol = " ";
					}
					if (boto.getText() == ("x^2")) {
						if (simbol.equals("x") || simbol.equals("-") || simbol.equals("+") || simbol.equals("/")) {
							char num[] = new char[(textField_1.getText().length()-1)];					
							for (int i = 0; i < (textField_1.getText().length()-1); i++) {
								num[i] = textField_1.getText().charAt(i);
							}
							textField.setText(new String(num));	
							if (textField_1.getText().equals("")) {
								textField.setText("0");
							}
							simbol = "x^2";
							resultat(operador1, operador2, simbol);
							
							textField_1.setText(textField.getText() + "^2 =");
							textField.setText(String.valueOf(resultat));
							historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
							textHistorial.setText(historial);
						}else {
							simbol = "x^2";
							resultat(operador1, operador2, simbol);
							
							textField_1.setText(textField.getText() + "^2 =");
							textField.setText(String.valueOf(resultat));
							historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
							textHistorial.setText(historial);
						}
						simbol = " ";
					}
					if (boto.getText() == ("%")) {
						if (simbol.equals("x") || simbol.equals("-") || simbol.equals("+") || simbol.equals("/")) {
							char num[] = new char[(textField_1.getText().length()-1)];					
							for (int i = 0; i < (textField_1.getText().length()-1); i++) {
								num[i] = textField_1.getText().charAt(i);
							}
							textField.setText(new String(num));	
							if (textField_1.getText().equals("")) {
								textField.setText("0");
							}
							simbol = "%";
							resultat(operador1, operador2, simbol);
							textField_1.setText(textField.getText() + "% =");
							textField.setText(String.valueOf(resultat));
							historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
							textHistorial.setText(historial);
						}else {
							simbol = "%";
							resultat(operador1, operador2, simbol);
							textField_1.setText(textField.getText() + "% =");
							textField.setText(String.valueOf(resultat));
							historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
							textHistorial.setText(historial);
						}
						simbol = " ";
					}
					if (boto.getText() == ("1/x")) {
						if (simbol.equals("x") || simbol.equals("-") || simbol.equals("+") || simbol.equals("/")) {
							char num[] = new char[(textField_1.getText().length()-1)];					
							for (int i = 0; i < (textField_1.getText().length()-1); i++) {
								num[i] = textField_1.getText().charAt(i);
							}
							textField.setText(new String(num));	
							if (textField_1.getText().equals("")) {
								textField.setText("0");
							}
							simbol = " ";
							if (Double.parseDouble(textField.getText()) == 0.00) {
								textField.setText("No és pot dividir un nombre per 0");
							}else {
							simbol = "1/x";
							resultat(operador1, operador2, simbol);
							textField_1.setText("1/" + textField.getText() + " =");
							textField.setText(String.valueOf(resultat));	
							historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
							textHistorial.setText(historial);
							}
						}else {
							if (Double.parseDouble(textField.getText()) == 0.00) {
								textField.setText("No és pot dividir un nombre per 0");
							}else {
							simbol = "1/x";
							resultat(operador1, operador2, simbol);
							textField_1.setText("1/" + textField.getText() + " =");
							textField.setText(String.valueOf(resultat));	
							historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
							textHistorial.setText(historial);
							}
						}
						simbol = " ";
						}
					if (boto.getText() == ("+/-")) {
						if (simbol.equals("x") || simbol.equals("-") || simbol.equals("+") || simbol.equals("/")) {
							char num[] = new char[(textField_1.getText().length()-1)];					
							for (int i = 0; i < (textField_1.getText().length()-1); i++) {
								num[i] = textField_1.getText().charAt(i);
							}
							textField.setText(new String(num));	
							if (textField_1.getText().equals("")) {
								textField.setText("0");
							}
							simbol = " ";
							double signe = Double.parseDouble(textField.getText())*(-1);
							textField_1.setText("-(" + textField.getText() + ") =");
							textField.setText(String.valueOf(signe));	
							historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
							textHistorial.setText(historial);
						}else {
							double signe = Double.parseDouble(textField.getText())*(-1);
							textField_1.setText("-(" + textField.getText() + ") =");
							textField.setText(String.valueOf(signe));	
							historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
							textHistorial.setText(historial);
						}
					}
					if (boto.getText() == ("=")) {
						if (!textField.getText().equals("")) {
								resultat(operador1, operador2, simbol);
								if (resultat == Double.POSITIVE_INFINITY){
									textField.setText("No és pot dividir un nombre per 0");
								}else {
								String resultString = String.valueOf(resultat);
								textField.setText(resultString);
								historial = textHistorial.getText().concat(textField_1.getText() + decimal.format(Double.parseDouble(textField.getText())) +"\n");
								textHistorial.setText(historial);
							}
							simbol = " ";
						}else {
							textField.setText("0");
							simbol = " ";
						}
					}
					
				}
			});
			
		}
	
	private void operadors (JButton boto) {
		if (boto.getText() == ("+") || boto.getText() == ("-") || boto.getText() == ("x") || boto.getText() == ("/")) {
			if (simbol.equals(" ")) {
			operador1 = Double.valueOf(textField.getText());
			simbol = boto.getText();
			textField_1.setText(operador1 + " " + simbol);
			}else {
				char num[] = new char[(textField_1.getText().length()-1)];					
				for (int i = 0; i < (textField_1.getText().length()-1); i++) {
					num[i] = textField_1.getText().charAt(i);
				}
				textField.setText(new String(num));	
				if (textField_1.getText().equals("")) {
					textField.setText("0");
				}
				operador1 = Double.valueOf(textField.getText());
				simbol = boto.getText();
				textField_1.setText(operador1 + " " + simbol);
			}
		} else if (boto.getText() == ("=")) {
			if (!textField.getText().equals("")) {
				operador2 =  Double.valueOf(textField.getText());
			}
		}
	}
	private void text(JButton boto) {
		if ((!(boto.getText()).equals("√x")) && (!(boto.getText()).equals("x^2")) && (!(boto.getText()).equals("1/x")) && (!(boto.getText()).equals("%")) && (!(boto.getText()).equals("+/-"))){
			if (textField.getText().equals("0")) {
				textField.setText(boto.getText());
			}else {				
				text = textField.getText().concat(boto.getText());
				textField.setText(text);
				}
		}
		
	}	
	
	public static double resultat(double operador1, double operador2, String simbol) {
		if (!textField.getText().equals("")) {
			
			switch (simbol) {
				case "+":
					resultat = operador1 + operador2;
					textField_1.setText(operador1 + " + " + operador2 + " =");
				break;
				case "-":
					resultat = operador1 - operador2;
					textField_1.setText(operador1 + " - " + operador2 + " =");
				break;
				case "x":
					resultat = operador1 * operador2;
					textField_1.setText(operador1 + " x " + operador2 + " =");
				break;
				case "/":
					resultat = operador1 / operador2;
					textField_1.setText(operador1 + " / " + operador2 + " =");
				break;
				case "%":
					resultat = Double.parseDouble(textField.getText())/100;
				break;
				case "√x":
					resultat = Math.sqrt(Double.parseDouble(textField.getText()));
				break;
				case "x^2":
					resultat = Math.pow(Double.parseDouble(textField.getText()),2);
				break;
				case "1/x":
					resultat = 1/Double.parseDouble(textField.getText());
				break;
			}
		}
		return resultat;
	}
	
	
}




