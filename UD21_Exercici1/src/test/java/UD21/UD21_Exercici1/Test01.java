package UD21.UD21_Exercici1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import views.AplicacioGrafica;


class Test01 {
	
	@Test
	void test() {
	}
	
	//Suma
	private static Stream<Arguments> getSuma(){
		return Stream.of(
				Arguments.of(3,2,5,"+"));
		}
		
	//Mètode test Suma
	@ParameterizedTest
	@MethodSource ("getSuma")
	public void testSuma (double a, double b, double c, String d) {
		AplicacioGrafica geo = new AplicacioGrafica();
		double result = geo.resultat(a,b,d);
		assertEquals(c,result);
	}
	
	//Resta
	private static Stream<Arguments> getResta(){
		return Stream.of(
				Arguments.of(3,2,1,"-"));
		}
		
	//Mètode test Resta
	@ParameterizedTest
	@MethodSource ("getResta")
	public void testResta (double a, double b, double c, String d) {
		AplicacioGrafica geo = new AplicacioGrafica();
		double result = geo.resultat(a,b,d);
		assertEquals(c,result);
	}
	
	//Multiplicacio
	private static Stream<Arguments> getMultiplicacio(){
		return Stream.of(
				Arguments.of(3,2,6,"x"));
	}
			
	//Mètode test Multiplicacio
	@ParameterizedTest
	@MethodSource ("getMultiplicacio")
	public void testMultiplicacio (double a, double b, double c, String d) {
		AplicacioGrafica geo = new AplicacioGrafica();
		double result = geo.resultat(a,b,d);
		assertEquals(c,result);
	}	

	
	//Divisio
	private static Stream<Arguments> getDivisio(){
		return Stream.of(
				Arguments.of(6,2,3,"/"));
	}
				
	//Mètode test Divisio
	@ParameterizedTest
	@MethodSource ("getDivisio")
	public void testDivisio (double a, double b, double c, String d) {
		AplicacioGrafica geo = new AplicacioGrafica();
		double result = geo.resultat(a,b,d);
		assertEquals(c,result);
	}	
	
	//Percentatge
	private static Stream<Arguments> getPercentatge(){
		return Stream.of(
				Arguments.of(9,0.09,"%"));
	}
					
	//Mètode test Percentatge
	@ParameterizedTest
	@MethodSource ("getPercentatge")
	public void testPercentatge (double a, double b, String c) {
		AplicacioGrafica geo = new AplicacioGrafica();
		geo.textField.setText(String.valueOf(a));
		double result = geo.resultat(a,b,c);
		assertEquals(b,result);
	}
	//Arrel
	private static Stream<Arguments> getArrel(){
		return Stream.of(
				Arguments.of(9,3,"√x"));
	}
					
	//Mètode test Arrel
	@ParameterizedTest
	@MethodSource ("getArrel")
	public void testArrel (double a, double b, String c) {
		AplicacioGrafica geo = new AplicacioGrafica();
		geo.textField.setText(String.valueOf(a));
		double result = geo.resultat(a,b,c);
		assertEquals(b,result);
	}	
	
	//Potencia
	private static Stream<Arguments> getPotencia(){
		return Stream.of(
				Arguments.of(9,81,"x^2"));
	}
						
	//Mètode test Potencia
	@ParameterizedTest
	@MethodSource ("getPotencia")
	public void testPotencia (double a, double b, String c) {
		AplicacioGrafica geo = new AplicacioGrafica();
		geo.textField.setText(String.valueOf(a));
		double result = geo.resultat(a,b,c);
		assertEquals(b,result);
	}
	
	//Inversa
	private static Stream<Arguments> getInversa(){
		return Stream.of(
				Arguments.of(2,0.5,"1/x"));
	}
							
	//Mètode test Inversa
	@ParameterizedTest
	@MethodSource ("getInversa")
	public void testInversa (double a, double b, String c) {
		AplicacioGrafica geo = new AplicacioGrafica();
		geo.textField.setText(String.valueOf(a));
		double result = geo.resultat(a,b,c);
		assertEquals(b,result);
	}
	
}
